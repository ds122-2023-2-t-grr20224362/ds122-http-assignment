Tecnologia em Análise e Desenvolvimento de Sistemas

Setor de Educação Profissional e Tecnológica - SEPT

Universidade Federal do Paraná - UFPR

---

*DS122 - Desenvolvimento de Aplicações Web 1*

Prof. Alexander Robert Kutzke

# Atividade: Mensagens HTTP

O servidor `web1.kutzke.com.br` está preparado para responder requisições baseadas no padrão [REST](https://developer.mozilla.org/en-US/docs/Glossary/REST).

Utilize o comando `curl` para interagir com o servidor `web1.kutzke.com.br` e alterar os registro por ele armazenados.

Ao final, preencha no arquivo `README.md`, cada uma dos comandos utilizados e a respectiva saída contendo as mensagens de requisição e resposta (é só colar a saída do comando `curl`).

O site responde às seguintes requisições:

| Verbo HTTP | Padrão UR   | Função                              |
|-----------:|-------------|-------------------------------------|
|        GET | /posts      | Lista posts (index)                 |
|       POST | /posts      | Cria um novo post (create)          |
|        GET | /posts/id   | Exibe o post número "id" (show)     |
|        PUT | /posts/id   | Altera o post número "id" (update)  |
|     DELETE | /posts/id   | Remove o post número "id" (delete)  |

## Exemplo com o método GET

```bash
curl -X GET -v web1.kutzke.com.br/posts   
```

## Respostas

```
curl -X GET -v web1.kutzke.com.br/posts   
```

```
Note: Unnecessary use of -X or --request, GET is already inferred.
* processing: web1.kutzke.com.br/posts
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0*   Trying 200.236.3.126:80...
* Connected to web1.kutzke.com.br (200.236.3.126) port 80
> GET /posts HTTP/1.1
> Host: web1.kutzke.com.br
> User-Agent: curl/8.2.1
> Accept: */*
>
< HTTP/1.1 503 Service Temporarily Unavailable
< Server: nginx
< Date: Thu, 14 Sep 2023 00:51:01 GMT
< Content-Type: text/html
< Content-Length: 190
< Connection: keep-alive
<
{ [190 bytes data]
100   190  100   190    0     0    591      0 --:--:-- --:--:-- --:--:--   591<html>
<head><title>503 Service Temporarily Unavailable</title></head>
<body>
<center><h1>503 Service Temporarily Unavailable</h1></center>
<hr><center>nginx</center>
</body>
</html>

* Connection #0 to host web1.kutzke.com.br left intact

```

curl -X POST -v web1.kutzke.com.br/posts   

```
COLE AQUI O COMANDO curl UTILIZADO
```

```
* processing: web1.kutzke.com.br/posts
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0*   Trying 200.236.3.126:80...
* Connected to web1.kutzke.com.br (200.236.3.126) port 80
> POST /posts HTTP/1.1
> Host: web1.kutzke.com.br
> User-Agent: curl/8.2.1
> Accept: */*
>
< HTTP/1.1 503 Service Temporarily Unavailable
< Server: nginx
< Date: Thu, 14 Sep 2023 00:52:37 GMT
< Content-Type: text/html
< Content-Length: 190
< Connection: keep-alive
<
{ [190 bytes data]
100   190  100   190    0     0   4867      0 --:--:-- --:--:-- --:--:--  5000<html>
<head><title>503 Service Temporarily Unavailable</title></head>
<body>
<center><h1>503 Service Temporarily Unavailable</h1></center>
<hr><center>nginx</center>
</body>
</html>

* Connection #0 to host web1.kutzke.com.br left intact

```

### POST (criar)

```
COLE AQUI O COMANDO curl UTILIZADO
```

```
COLE AQUI A SAÍDA DO COMANDO curl UTILIZADO
```

### GET com id (mostrar um registro específico)

```
COLE AQUI O COMANDO curl UTILIZADO
```

```
COLE AQUI A SAÍDA DO COMANDO curl UTILIZADO
```

### PUT com id (alterar um registro específico)

```
COLE AQUI O COMANDO curl UTILIZADO
```

```
COLE AQUI A SAÍDA DO COMANDO curl UTILIZADO
```

### DELETE com id (apagar um registro específico)

```
COLE AQUI O COMANDO curl UTILIZADO
```

```
COLE AQUI A SAÍDA DO COMANDO curl UTILIZADO
```

